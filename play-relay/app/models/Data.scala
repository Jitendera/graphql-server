package models


import sangria.schema._
import sangria.execution._
import sangria.macros._
//import sangria.marshalling.sprayJson._

class UserRepo {
  import scala.collection.mutable.{Map ⇒ MMap}

  val years: List[String] =
    List("1974", "1975", "1976", "1977", "1978", "1979", "1980")

  val make = MMap[String, List[String]](
    "2015" → List("Acura", "Alfa Romeo", "Aprilia"),
    "2016" → List("Volvo", "Yamaha")
  )

  val model = MMap[String, List[String]](
    "Acura" → List("ILX", "MDX", "RDX", "RLX")
  )

  val submodel=MMap[String, List[String]](
      "TLX" -> List("Base", "Dynamic", "Hybrid"),
      "RDX" -> List("Dynamic", "Hybrid")
  )
  val engine=MMap[String, List[String]](
      "Base" ->    List("4a2.0", "4a2.4", "4a2.5"),
      "Dynamic" -> List("4a2.4"),
      "Hybrid" -> List("4a2.0", "4a2.4")
  )
}








/*
import sangria.schema.{Deferred, DeferredResolver}

import scala.concurrent.Future
import scala.util.Try

case class User( year:List[String], make:List[String], model:List[String])
//case class User(ur:UserRepo, make:Option[String]=None, model:Option[String]=None)

class UserRepo{
  import scala.collection.mutable.{Map => MMap}
  val years:List[String]=List("1974","1975","1976","1977","1978","1979","1980")
  val make=MMap[String,List[String]](
      "2015" -> List("Acura", "Alfa Romeo", "Aprilia"),
      "2016" -> List("Volvo", "Yamaha")
      )
  val model=MMap[String,List[String]](
      "Acura" -> List("ILX", "MDX", "RDX", "RLX")
      )
  val map = MMap[String,User](
    "101" -> User("101","2000","make","Hero","Honda"),
    "102"->User("102","2001","make","Maruti","Civic")
  )

  //def byId(id: String) = map(id)
           
  def store(mak:String, mod:String)={
    def getYear()=years
      def MakeF(mak:String)=make(mak)
      def ModelF(mod:String)=model(mod)
  } 
      
      def store(mak:String, mod:String)= {
  User(years, make(mak), model(mod))
}
      
  def loginPlusPlus(email: String) = {
    val user = map(email)
    map += (email -> user.copy(loginCount = user.loginCount + 1))
    map(email)
  }
}*/