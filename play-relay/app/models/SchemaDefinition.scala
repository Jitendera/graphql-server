package models

import sangria.schema._

import scala.concurrent.Future

import sangria.schema._
import sangria.execution.Executor
import sangria.parser.QueryParser
import sangria.renderer.SchemaRenderer
import scala.concurrent.ExecutionContext.Implicits.global
import javax.xml.soap.Detail


/**
 * Defines a GraphQL schema for the current project
 */
object SchemaDefinition {
  val YearArg = Argument("year", StringType)
val MakeArg = Argument("make", StringType)
  val ModelArg= Argument("model", StringType)
  val SubmodelArg= Argument("submodel", StringType)

val StoreType = ObjectType("Store", List[Field[UserRepo, Unit]](
  Field("years", ListType(StringType), resolve = _.ctx.years),
  Field("makes", ListType(StringType),
    arguments = YearArg :: Nil,
    resolve = c ⇒ c.ctx.make.getOrElse(c arg YearArg, Nil)),
  Field("model", ListType(StringType),
    arguments = MakeArg :: Nil,
    resolve = c ⇒ c.ctx.model.getOrElse(c arg MakeArg, Nil)),
  Field("submodel", ListType(StringType),
    arguments = ModelArg :: Nil,
    resolve = c ⇒ c.ctx.submodel.getOrElse(c arg ModelArg, Nil)),
  Field("engine", ListType(StringType),
    arguments = SubmodelArg :: Nil,
    resolve = c ⇒ c.ctx.engine.getOrElse(c arg SubmodelArg, Nil))
))

val Query = ObjectType("Query", List[Field[UserRepo, Unit]](
  Field("store", StoreType, resolve = _ ⇒ ())
))

val schema = Schema(Query)
  
}
